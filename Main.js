const ANTS_NUM = 10;
const CITIES_NUM = 6;
class Main {
    constructor() {
        this.ants = [];
        this.cities = [];
        this.roads = [];
        this.init = this.init.bind(this);
    }

    init() {
        const ants = this.ants;
        const cities = this.cities;
        for (let i = 0; i < CITIES_NUM; i++) {
            cities.push(new City('city' + i));
        }
        for (let i = 0; i < ANTS_NUM; i++) {
            const city = cities[i % CITIES_NUM];
            ants.push(new Ant('ant' + i, city));
        }
        this.roads.push(cities[0].linkTo(cities[1], 7));
        this.roads.push(cities[0].linkTo(cities[2], 3));
        this.roads.push(cities[1].linkTo(cities[5], 7));
        this.roads.push(cities[1].linkTo(cities[4], 4));
        this.roads.push(cities[2].linkTo(cities[3], 2));
        this.roads.push(cities[3].linkTo(cities[4], 3));
        this.roads.push(cities[3].linkTo(cities[5], 3));
        this.roads.push(cities[2].linkTo(cities[4], 5));
    };
    start(){
        const ants = this.ants;
        const cities = this.cities;
        for (let i = 0; i < 100; i++) {
            this.logTraces();
            ants.forEach(function (ant) {
                ant.move();
            });
            this.logTraces();
            this.roads.forEach(function (r) {
                r.trace = r.trace * 0.5;
            });
        }
        console.log('final');
        this.logTraces();
    };
    logTraces() {
        this.roads.forEach(function (r) {
            console.log(r.cities[0].name, r.cities[1].name, r.trace);
        });
    };
}
class Ant {
    constructor(name, city) {
        this.citiesHistory = [];
        this.name = name;
        this.city = city;
        city.ants.push(this);
    }
    move() {
        const roadToGo = this.selectRoadToGo();
        const cityToGo = this.getAnotherCityByRoad(roadToGo);
        console.log(this.name + ' go to ' + cityToGo.name + ' from ' + this.city.name, roadToGo.trace + 1);
        roadToGo.trace++;
        this.city.removeAnt(this);
        this.citiesHistory.push(this.city);
        this.city = cityToGo;
    };
    selectRoadToGo() {
        const _this = this;
        const roads = this.city.roads;
        const weightedRoads = roads.map(function (r) {
            const anotherCity = _this.getAnotherCityByRoad(r);
            let weight = (r.trace) / (r.len);
            if (_this.citiesHistory.filter(function (c) { return c === anotherCity; }).length >= 3) {
                weight = 0;
            }
            return { item: r, weight: weight };
        });
        const randRoad = weightedRand(weightedRoads);
        return randRoad;
    };
    getAnotherCityByRoad(road) {
        const _this = this;
        if (!road.cities.includes(this.city))
            throw Error('ошибка 1');
        return road.cities.find(function (c) { return c !== _this.city; });
    };
}
class City{
    constructor(name) {
        this.roads = [];
        this.ants = [];
        this.name = name;
    }
linkTo(city, len) {
        if (len === void 0) { len = 1; }
        const road = new Road(this, city, len);
        if (this.roads.some(function (r) { return r.cities.includes(city); }))
            throw Error('Город уже с дорогой');
        this.roads.push(road);
        city.roads.push(road);
        return road;
    };
removeAnt(ant) {
        const pos = this.ants.indexOf(ant);
        if (pos !== -1)
            this.ants.splice(pos, 1);
    };
}
class Road{
    constructor(c1, c2, len) {
        this.trace = 1;
        this.len = len;
        this.cities = [c1, c2];
    }
};
function weightedRand(options) {
    const weights = [];
    for (let i = 0; i < options.length; i++)
        weights[i] = options[i].weight + (weights[i - 1] || 0);
    const random = Math.random() * weights[weights.length - 1];
    let retIndex = options.length - 1;
    for (let i = 0; i < weights.length; i++)
        if (weights[i] > random) {
            retIndex = i;
            break;
        }
    return options[retIndex].item;
}
const main = new Main();
main.init();
main.start();
